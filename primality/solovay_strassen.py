import random
import numpy
import math


def get_divisor(a):
    for i in range(2, math.floor(math.sqrt(a)) + 2):
        if a % i == 0:
            return i


def compute_jacobi(a, n):
    a = int(a)
    n = int(n)
    # R1:
    if a == 1:
        return 1

    # R2:
    if a == 2:
        if n % 8 == 1 or n % 8 == 7:
            return 1
        else:
            return -1

    # R4:
    if a > n:
        return compute_jacobi(a % n, n)

    # R3:
    if a % 2 == 0:
        return compute_jacobi(2, n) * compute_jacobi(a / 2, n)

    # R5:
    print(a)
    print(n)
    if numpy.gcd(a, n) == 1:
        if a % 4 == 1 or n % 4 == 1:
            return compute_jacobi(n, a)
        if a % 4 == 3 and n % 4 == 3:
            return -1 * compute_jacobi(n, a)
    else:
        div = get_divisor(a)
        return compute_jacobi(div, n) * compute_jacobi(a / div, n)


def test_solovay_strassen(number):
    if number == 2:
        return True
    if number % 2 == 0:
        return False
    number = abs(number)
    if number < 2:
        return False
    a = random.randint(2, number - 1)
    a = int(a)
    number = int(number)
    if numpy.gcd(number, a) != 1:
        return False
    else:
        r = pow(a, (number - 1) // 2, number)
        if r != 1 and r != number - 1:
            return False
        else:
            s = compute_jacobi(a, number) % number
            if s != r:
                return False
            else:
                return True


# print(compute_jacobi(1236, 20003))
print(test_solovay_strassen(29))