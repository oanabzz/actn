import numpy
import math


def check_prime(n):
    if n == 2:
        return True
    for i in range(2,math.floor(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True


def compute_modulo(s, mn, n):
    string_format = "{0:0" + str(2 * n) + "b}"
    binary_string = string_format.format(s)
    x1 = int(binary_string[:n], 2)
    x0 = int(binary_string[n:], 2)

    if x1 + x0 >= mn:
        return x1 + x0 - mn
    else:
        return x1 + x0


def test_lucas_lehmer(n):
    mn = pow(2, n) - 1
    if not check_prime(n):
        return False
    s = 4
    for i in range(2, n):
        s = s ** 2 - 2
        s = compute_modulo(s, mn, n)
    if s == 0:
        return True
    else:
        return False


print(test_lucas_lehmer(11213))
