import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class AdditionChainGenerator {
    public static AdditionChain getChain(BigInteger number) {
        List<BigInteger> result = new LinkedList<>();
        List<Integer> firstIndex = new LinkedList<>();
        firstIndex.add(-1);
        firstIndex.add(0);

        List<Integer> secondIndex = new LinkedList<>();
        secondIndex.add(-1);
        secondIndex.add(0);

        result.add(BigInteger.ONE);
        result.add(BigInteger.TWO);
        while (result.get(result.size() - 1).multiply(BigInteger.TWO).compareTo(number) <= 0) {
            firstIndex.add(result.size()-1);
            secondIndex.add(result.size()-1);
            result.add(result.get(result.size() - 1).multiply(BigInteger.TWO));
        }
        if (result.get(result.size() - 1).compareTo(number) == 0) {
            return new AdditionChain(result, firstIndex, secondIndex);
        }
        while (result.get(result.size() - 1).compareTo(number) != 0) {
            BigInteger last = result.get(result.size() - 1);
            for (int i = result.size() - 2; i >= 0; i--) {
                if (last.add(result.get(i)).compareTo(number) <= 0) {
                    firstIndex.add(result.size()-1);
                    secondIndex.add(i);
                    result.add(last.add(result.get(i)));
                    break;
                }
            }
        }
        return new AdditionChain(result, firstIndex, secondIndex);
    }
}
