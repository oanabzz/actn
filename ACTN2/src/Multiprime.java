import java.math.BigInteger;
import java.util.Random;

@SuppressWarnings("Duplicates")
public class Multiprime {
    private BigInteger n;
    private BigInteger p;
    private BigInteger q;
    private BigInteger r;
    private BigInteger d;
    public static BigInteger e = BigInteger.TWO.pow(16).add(BigInteger.ONE);

    public Multiprime() {
        Random random = new Random();
        this.p = BigInteger.probablePrime(512, random);
        this.q = BigInteger.probablePrime(512, random);
        this.r = BigInteger.probablePrime(512, random);
        this.n = p.multiply(q).multiply(r);
        this.d = e.modInverse(
                p.subtract(BigInteger.ONE)
                        .multiply(q.subtract(BigInteger.ONE))
                        .multiply(r.subtract(BigInteger.ONE))
        );
    }

    private BigInteger solveTCR(BigInteger mp, BigInteger mq, BigInteger mr) {
        BigInteger x1 = mp;
        BigInteger alpha = mq.subtract(x1).multiply(p.modInverse(q)).mod(q);
        BigInteger x2 = x1.add(alpha.multiply(p));
        alpha = mr.subtract(x2).multiply(p.multiply(q).modInverse(r)).mod(r);
        return x2.add(alpha.multiply(p).multiply(q)).mod(p.multiply(q).multiply(r));


//        BigInteger cp = q.multiply(r);
//        BigInteger cq = p.multiply(r);
//        BigInteger cr = p.multiply(q);
//
//        BigInteger xp = mp.multiply(cp.modInverse(p)).mod(p);
//        BigInteger xq = mq.multiply(cq.modInverse(q)).mod(q);
//        BigInteger xr = mr.multiply(cr.modInverse(r)).mod(r);
//
//        BigInteger sum = xp.multiply(q).multiply(r)
//                .add(xq.multiply(p).multiply(r))
//                .add(xr.multiply(p).multiply(q))
//                .mod(n);
//        return sum;
    }

    public BigInteger decrypt(BigInteger cipher) {
        BigInteger mp = cipher.mod(this.p)
                .modPow(d.mod(p.subtract(BigInteger.ONE)), p);
        BigInteger mq = cipher.mod(this.q).modPow(d.mod(q.subtract(BigInteger.ONE)), q);
        BigInteger mr = cipher.mod(this.r).modPow(d.mod(r.subtract(BigInteger.ONE)), r);

        return solveTCR(mp, mq, mr);
    }

    public BigInteger decodeWithAdditionChians(BigInteger cipher) {
        AdditionChain pChain = AdditionChainGenerator.getChain(d.mod(p.subtract(BigInteger.ONE)));
        AdditionChain qChain = AdditionChainGenerator.getChain(d.mod(q.subtract(BigInteger.ONE)));
        AdditionChain rChain = AdditionChainGenerator.getChain(d.mod(r.subtract(BigInteger.ONE)));


        BigInteger mp = pChain.computeModularExp(cipher.mod(p),p);
        BigInteger mq = qChain.computeModularExp(cipher.mod(q),q);
        BigInteger mr = rChain.computeModularExp(cipher.mod(r),r);

        return solveTCR(mp, mq, mr);
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getR() {
        return r;
    }

    public BigInteger getD() {
        return d;
    }
}
