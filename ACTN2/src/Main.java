import java.math.BigInteger;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Multiprime multiprime = new Multiprime();
        BigInteger e = Multiprime.e;
        BigInteger n = multiprime.getN();

        BigInteger message = BigInteger.probablePrime(512, random);
        System.out.println("Message: \t\t\t\t\t\t\t\t\t" + message);

        BigInteger cipher = message.modPow(e, n);

        Long startMultiprime1 = System.currentTimeMillis();
        BigInteger decryption = multiprime.decrypt(cipher);
        Long endMultiprime1 = System.currentTimeMillis();

        System.out.println("Multiprime decryption: \t\t\t\t\t\t" + decryption);

        Long startMultiprime2 = System.currentTimeMillis();
        decryption = multiprime.decodeWithAdditionChians(cipher);
        Long endMultiprime2 = System.currentTimeMillis();

        System.out.println("Multiprime decription with addition chains:\t" + decryption);

        Multipower multipower = new Multipower();
        BigInteger n2 = multipower.getN();
        BigInteger cipher2 = message.modPow(e, n2);

        Long startMultipower = System.currentTimeMillis();
        decryption = multipower.decode(cipher2);
        Long endMultipower = System.currentTimeMillis();
        System.out.println("Multipower decryption: \t\t\t\t\t\t" + decryption);

        Long startLibrary1 = System.currentTimeMillis();
        cipher.modPow(multiprime.getD(), multiprime.getN());
        Long endLibrary1 = System.currentTimeMillis();

        Long startLibrary2 = System.currentTimeMillis();
        cipher2.modPow(multipower.getD(), multipower.getN());
        Long endLibrary2 = System.currentTimeMillis();

        System.out.println("\nLibrary time for Multiprime: " + (endLibrary1 - startLibrary1));
        System.out.println("Multiprime alg time: " + (endMultiprime1 - startMultiprime1));
        System.out.println("Multiprime alg with addition chains time: " + (endMultiprime2 - startMultiprime2));

        System.out.println("\nLibrary time for Multipower: " + (endLibrary2 - startLibrary2));
        System.out.println("Multipower alg time: " + (endMultipower - startMultipower));

    }
}
