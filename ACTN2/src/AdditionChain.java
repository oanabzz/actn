import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class AdditionChain {
    private List<BigInteger> elements;
    private List<Integer> first;
    private List<Integer> last;

    public BigInteger computeModularExp(BigInteger a, BigInteger m) {
        List<BigInteger> partialRes = new LinkedList<>();
        partialRes.add(a.mod(m));
        for (int i = 1; i < this.elements.size(); i++) {
            BigInteger result = partialRes.get(first.get(i)).multiply(partialRes.get(last.get(i))).mod(m);
            partialRes.add(result);
        }
        return partialRes.get(partialRes.size() - 1);
    }

    public AdditionChain() {

    }

    public AdditionChain(List<BigInteger> elements, List<Integer> first, List<Integer> last) {
        this.elements = elements;
        this.first = first;
        this.last = last;
    }

    public List<BigInteger> getElements() {
        return elements;
    }

    public List<Integer> getFirst() {
        return first;
    }

    public List<Integer> getLast() {
        return last;
    }

    public void setElements(List<BigInteger> elements) {
        this.elements = elements;
    }

    public void setFirst(List<Integer> first) {
        this.first = first;
    }

    public void setLast(List<Integer> last) {
        this.last = last;
    }

    @Override
    public String toString() {
        return "AdditionChain{" +
                "\n" + elements +
                ", \n" + first +
                ", \n" + last +
                '}';
    }
}
