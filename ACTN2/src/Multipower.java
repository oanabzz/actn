import java.math.BigInteger;
import java.util.Random;

public class Multipower {
    private BigInteger p;
    private BigInteger q;
    private static final BigInteger e = BigInteger.TWO.pow(16).add(BigInteger.ONE);
    private Double intE = Math.pow(1, 16) + 1;
    private BigInteger d;

    public Multipower() {
        Random random = new Random();
        this.p = BigInteger.probablePrime(512, random);
        this.q = BigInteger.probablePrime(512, random);
        BigInteger phi = p.multiply(p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
        this.d = e.modInverse(phi);
    }

    private BigInteger solveTCR(BigInteger b1, BigInteger b2) {
        BigInteger c1 = q;
        BigInteger c2 = p.multiply(p);

        BigInteger x1 = c1.modInverse(p.multiply(p)).multiply(b1).mod(p.multiply(p));
        BigInteger x2 = c2.modInverse(q).multiply(b2).mod(q);

        BigInteger sum = x1.multiply(c1).mod(p.multiply(p).multiply(q))
                .add(x2.multiply(c2)).mod(p.multiply(p).multiply(q));

        return sum;
    }

    public BigInteger getD() {
        return d;
    }

    public BigInteger decode(BigInteger cipher) {
        BigInteger m0 = cipher.mod(p).modPow(d.mod(p.subtract(BigInteger.ONE)), p);
        BigInteger auxInv = e.multiply(m0.modPow(e.subtract(BigInteger.ONE), p)).mod(p).modInverse(p);
        BigInteger m1 = cipher.subtract(m0.modPow(e, p.multiply(p))).mod(p.multiply(p)).divide(p).multiply(auxInv).mod(p);

        BigInteger mp2 = m1.multiply(p).add(m0);
        BigInteger mq = cipher.mod(q).modPow(d.mod(q.subtract(BigInteger.ONE)), q).mod(q);

        return solveTCR(mp2, mq);
    }

    public BigInteger getN() {
        return p.multiply(p).multiply(q);
    }
}
