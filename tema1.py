import binascii
from Crypto.Util import number
import numpy as np
import itertools


# k = 3


def get_p():
    n_length = 100
    return number.getPrime(n_length)


# def transform_input(input):
#     result = str(bin(int(binascii.hexlify(input.encode()), 16)))[2:]
#     return result
#     # while len(result) % 160 != 0:
#     #     result = result + '0'
#     # return result
#
#
# # print(transform_input('ab')[2:])
# # n = int(transform_input('hello'), 2)
# # print(binascii.unhexlify('%x' % n))
#
#
# # print(bin(get_p()))
#
#
# # TODO: len(input) divizibil cu 160
# def get_blocks(input):
#     blocks = []
#     nr = len(input) // 160
#     for i in range(0, nr + 1):
#         block = input[160 * i: 160 * (i + 1)]
#         blocks.append(block)
#     return blocks


# TODO: send k-1 blocks here
def compute_polynomial(input, value, k, p):
    poly = input[0] * value
    for i in range(1, k - 1):
        poly = ((poly + input[i]) * value) % p
    return poly


def encode(input_list, k, p):
    n = k + 2
    encoding = []
    for i in range(1, n + 1):
        encoding.append(compute_polynomial(input_list, i, k, p))
    return encoding


# input = "verysafepasswordverysafepasswordverysafepasswordverysafepasswordverysafepasswordverysafepasswordverysafepasswordverysafepasswordverysafepasswordverysafepassword"
# print(len(input))
# # p = get_p()
# blocks = get_blocks(transform_input(input))
# print(blocks)
# y = []
# for i in range(1, k + 3):
#     y.append(compute_polynomial(blocks[0:5], i, p))
# return y
# print(y)
# input = "abcd"
# print(binascii.a2b_uu(ord('a')))
# print(transform_input(input))
# print((len(transform_input(input))+1)/4)
# blocks = get_blocks(transform_input(input))


# encode()


# for s in iter_bin("Spam"):
#     print(s)


# verified
def compute_common_denominator(subset):
    return np.prod([i - j for i in subset for j in subset if i != j])


def compute_denominator(i, subset):
    return np.prod([j - i for j in subset if j != i])


def compute_numerator_product(i, subset):
    return np.prod(subset) / i


def compute_free_coefficient(output, subset, p):
    subset_product = np.prod(subset)
    common = compute_common_denominator(subset)

    inv = pow(int(common), p - 2, p)

    fc = np.sum(
        [output[subset[i] - 1] * (subset_product / subset[i]) * (common / compute_denominator(subset[i], subset)) for i
         in
         range(0, len(subset))]) % p

    return fc * inv


# def decode():
def find_subsets(S, m):
    return list(itertools.combinations(S, m))


# subset = [1, 2, 3, 4]
# print(find_subsets(subset, 2))


def compute_coefficients(a):
    coef = [0, 0]
    coef[-1] = -a[0]
    coef[-2] = 1
    length = 2
    for j in range(1, len(a)):
        length += 1
        t1 = list(coef)
        t1.append(0)
        t2 = list([0])
        t2.extend(list(np.multiply(coef, -a[j])))
        coef = list(np.add(t1, t2))
    return coef


def decode(output, p, k):
    subsets = find_subsets([i for i in range(1, k + 3)], k)

    print(subsets)

    i = 0
    fc = compute_free_coefficient(output, subsets[0], p)
    while fc != 0:
        i += 1
        print(i)
        fc = compute_free_coefficient(output, subsets[i], p)

    # i-ul pentru care a mers
    subset = list(subsets[i])

    # subset = [1, 3, 4]

    print(subset)

    common_denomitor = compute_common_denominator(subset)
    print(common_denomitor)
    coef = list(np.zeros(k))
    for i in range(0, len(subset)):
        aux = list(subset)
        aux.pop(i)
        current_coef = compute_coefficients(aux)
        current_coef = list(np.mod([output[subset[i] - 1] * el for el in current_coef], p))
        current_coef = list(
            np.mod(np.multiply(common_denomitor / compute_denominator(subset[i], subset), current_coef), p))
        coef = np.add(coef, current_coef)

    inv = pow(int(common_denomitor), p - 2, p)
    coef = list(np.mod(np.multiply(coef, inv), p))
    print(coef)
    return coef


# output = [9, 2, 6, 5, 8]
# k = 3
# p = 11

# decode(output, p, k)


def reed_solomon():
    # p = get_p()
    k = 5
    p = get_p()
    print("p: ", p)
    message = [get_p() // 100, get_p() // 100, get_p() // 100, get_p() // 100]
    print("message: ",message)
    encoding = encode(message, k, p)
    print("encoding ", encoding)
    encoding[3] = get_p() // 100
    result = decode(encoding, p, k)
    print(result)


reed_solomon()
